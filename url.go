package main

import (
	"fmt"
	"net/url"
	"strings"
)

func normalizeURL(u string) (string, error) {
	// XXX: replace \ with / (except in query and fragment)
	// XXX: add scheme if omitted
	// XXX: <input type=url> on client-side should prevent those already.

	pu, err := url.Parse(u)
	if err != nil {
		return "", fmt.Errorf("Malformed URL %q: %v", u, err)
	}
	if (pu.Scheme != "http" && pu.Scheme != "https") || pu.Host == "" {
		return "", fmt.Errorf("URL must be an absolute HTTP(S) reference: %q", u)
	}

	pu.Host = strings.ToLower(pu.Host) // TODO: IDNA/punycode
	if pu.Scheme == "http" && strings.HasSuffix(pu.Host, ":80") {
		pu.Host = pu.Host[:len(pu.Host)-3]
	} else if pu.Scheme == "https" && strings.HasSuffix(pu.Host, ":443") {
		pu.Host = pu.Host[:len(pu.Host)-4]
	} else if pu.Host[len(pu.Host)-1] == ':' {
		pu.Host = pu.Host[:len(pu.Host)-1]
	}
	if pu.Path == "" {
		pu.Path = "/"
		pu.RawPath = ""
	} // TODO: clean path (dot segments), normalize %-encoding
	return pu.String(), nil
}
