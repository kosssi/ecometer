/* eslint import/no-commonjs:off */
/* eslint-env node */

module.exports = {
  'env': {
    'phantomjs': true,
    'es6': true,
  },
  'plugins': [
    'compat',
  ],
  'rules': {
    // compat plugin
    'compat/compat': 'error',
  },
  'settings': {
    'browsers': [ 'last 2 Firefox versions' ],
    // PhantomJS core modules; 'fs' and 'child_process' are also Node core modules so need not be redefined here
    'import/core-modules': [ 'system', 'webpage', 'webserver' ],
  },
};
