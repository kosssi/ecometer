import { OK, NOK, NA } from '../status.js';
import { debug, forEachFrame, findResourceByUrl } from '../helpers.js';
// Check "Javascript minification" rule
// See https://gitlab.com/ecoconceptionweb/ecometer/issues/15
export const name = '88-javascript-minification';
export function check(page, resources) {

  // Search 'pristine' JS files
  const jsResources = Object.keys(resources).reduce(function(memo, resourceId) {
    const resource = resources[resourceId];
    if (!resource.res) return memo;
    const contentType = resource.res.contentType || '';
    if (contentType.toLowerCase().indexOf('javascript') !== -1) memo[resource.req.url] = resource;
    return memo;
  }, {});

  // Search 'sneaky' files (without proper content type)
  const scriptUrls = [];
  forEachFrame(page, () => {
    scriptUrls.push.apply(scriptUrls, page.evaluate(function() {
      let scriptTags = document.getElementsByTagName('script');
      scriptTags = Array.prototype.slice.call(scriptTags);
      return scriptTags.reduce(function(memo, script) {
        if (script.src) memo.push(script.src);
        return memo;
      }, []);
    }));
  });

  scriptUrls.forEach(function(scriptUrl) {
    if (!(scriptUrl in jsResources)) {
      const res = findResourceByUrl(resources, scriptUrl);
      if (res) jsResources[scriptUrl] = res;
    }
  });

  debug(() => `JS resources found: ${scriptUrls.join(', ')}`);

  let totalScripts = scriptUrls.length;
  let minifiedScripts = 0;

  scriptUrls.forEach(function(url) {
    const resource = jsResources[url];
    debug(() => `Detecting script minification "${url}"`);
    if (isMinified(resource.res.body)) minifiedScripts++;
  });

  const ratio = (minifiedScripts/totalScripts*100);
  const status = totalScripts === 0 ? NA : ( ratio > 95 ? OK : NOK );

  return {
    id: 88,
    slug: 'javascript-minification',
    status: status,
    data: { minifiedScripts, totalScripts, ratio },
  };

  function countChar(char, str) {
    let total = 0;
    for (let curr, i = 0; (curr = str[i]); i++) {
      if (curr === char) total++;
    }
    return total;
  }

  function isMinified(scriptContent) {

    if (!scriptContent) return true;

    const total = scriptContent.length-1;
    const semicolons = countChar(';', scriptContent);
    const linebreaks = countChar('\n', scriptContent);

    // Empiric method to detect minified files
    //
    // javascript code is minified if, on average:
    //  - there is more than one semicolon by line
    //  - and there are more than 100 characters by line
    const isMinified = semicolons/linebreaks > 1 && linebreaks/total < 0.01;

    debug(() => `S/L ${(semicolons/linebreaks).toFixed(2)} L/T ${(linebreaks/total).toFixed(2)} Minified: ${isMinified}`);

    return isMinified;

  }

}

export default { name, check };
