import { debug, isCompressible } from '../helpers';
import { OK, NOK, NA } from '../status.js';
// Check "HTTP Compression" rule
// See https://gitlab.com/ecoconceptionweb/ecometer/issues/14
export const name = '79-http-compress';
export function check(page, resources) {

  let resource, headers;
  let totalReceivedTextResources = 0;
  let compressedTextResources = 0;

  // From IANA official values
  // See http://www.iana.org/assignments/http-parameters/http-parameters.xhtml#content-coding
  const httpCompressionTokens = [
    'br',
    'compress',
    'deflate',
    'gzip',
    'pack200-gzip',
  ];

  for (let resourceId in resources) {
    if (resources.hasOwnProperty(resourceId)) {

      resource = resources[resourceId];

      debug(() => `The resource "${resource.req.url}" content-type is ${resource.res.contentType}. Body size: ${resource.res.bodySize}`);

      if (!resource.res ||
          resource.res.bodySize <= 150 || // Inspired by Chrome Dev Tools: https://github.com/ChromeDevTools/devtools-frontend/blob/6826279c6c64d50c88f1695c57d121227d7927ef/front_end/audits/AuditRules.js#L138
          !isCompressible(resource.res.contentType)) continue;

      debug(() => `The resource "${resource.req.url}" is considered compressible.`);

      totalReceivedTextResources++;

      headers = resource.res.headers;
      for (let h, i = 0; (h = headers[i]); i++) {
        if (h.name === 'Content-Encoding' && httpCompressionTokens.indexOf(h.value) !== -1) {
          debug(() => `The resource "${resource.req.url}" is compressed.`);
          compressedTextResources++;
          break;
        }
      }

    }
  }

  const ratio = compressedTextResources/totalReceivedTextResources*100;
  debug(() => `Compressed text resources ratio ${ratio.toFixed(1)}%`);

  const status = totalReceivedTextResources === 0 ? NA : ( ratio >= 95 ? OK : NOK );

  return {
    slug: 'http-compress',
    id: 79,
    status: status,
    data: {
      compressedTextResources: compressedTextResources,
      totalReceivedTextResources: totalReceivedTextResources,
      ratio: ratio,
    },
  };

}

export default { name, check };
