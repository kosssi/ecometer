import rule7ExternalizeCssAndJs from './7-externalize-css-and-js.js';
import rule10LimitCss from './10-limit-css.js';
import rule12CssMinification from './12-css-minification.js';
import rule16PrintCss from './16-print-css.js';
import rule19UseStandardTypefaces from './19-use-standard-typefaces.js';
import rule23EmptySrc from './23-empty-src.js';
import rule24LimitPlugins from './24-limit-plugins.js';
import rule35ValidateJs from './35-validate-js.js';
import rule78CacheHeaders from './78-cache-headers.js';
import rule79HttpCompress from './79-http-compress.js';
import rule80ETagHeader from './80-etag-header.js';
import rule88JavascriptMinification from './88-javascript-minification.js';
import rule93DontResizeImageInBrowser from './93-dont-resize-image-in-browser.js';
import rule170MinimizeNumberOfDomains from './170-minimize-number-of-domains.js';
import rule180LimitHttpRequests from './180-limit-http-requests.js';

export default [
  rule7ExternalizeCssAndJs,
  rule10LimitCss,
  rule12CssMinification,
  rule16PrintCss,
  rule19UseStandardTypefaces,
  rule23EmptySrc,
  rule24LimitPlugins,
  rule35ValidateJs,
  rule78CacheHeaders,
  rule79HttpCompress,
  rule80ETagHeader,
  rule88JavascriptMinification,
  rule93DontResizeImageInBrowser,
  rule170MinimizeNumberOfDomains,
  rule180LimitHttpRequests,
];
