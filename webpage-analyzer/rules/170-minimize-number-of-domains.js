import { debug } from '../helpers.js';
import { OK, NOK, NA } from '../status.js';
// See https://gitlab.com/ecoconceptionweb/ecometer/issues/36
export const name = '170-minimize-number-of-domains';
export function check(page, resources) {

  const domains = new Set();
  const resourceKeys = Object.keys(resources);

  for (let resId of resourceKeys) {

    const resource = resources[resId];
    const url = new URL(resource.req.url);
    const domain = url.host;

    debug(() => `Adding domain ${domain} for resource ${url}`);
    domains.add(domain);

  }

  return {
    slug: 'minimize-number-of-domains',
    id: 170,
    status: resourceKeys.length === 0 ? NA : ( domains.size <= 2 ? OK : NOK ),
    data: {
      domains: Array.from(domains),
    },
  };

}

export default { name, check };
