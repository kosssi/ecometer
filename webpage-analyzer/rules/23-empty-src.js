import { OK, NOK } from '../status.js';
// See https://gitlab.com/ecoconceptionweb/ecometer/issues/17
export const name = '23-empty-src';
export function check(page) {
  const countPerTag = page.evaluate(function() {
    return {
      'img':    document.querySelectorAll('img[src=""]').length,
      'script': document.querySelectorAll('script[src=""]').length,
      'link':   document.querySelectorAll('link[rel=stylesheet][href=""]').length,
    };
  });
  const passed = Object.keys(countPerTag)
        .map(function(key) {
          return countPerTag[key];
        })
        .reduce(function(prev, cur) {
          return prev + cur;
        }, 0) === 0;

  return {
    slug: 'empty-src',
    id: 23,
    status: passed ? OK : NOK,
    data: countPerTag,
  };
}

export default { name, check };
