import { OK, NOK, NA } from '../status.js';
import { isAsset, isResourceCompleted, debug } from '../helpers.js';
// See https://gitlab.com/ecoconceptionweb/ecometer/issues/17
export const name = '78-cache-headers';
export function check(page, resources) {

  let totalStaticResources = 0;
  let totalStaticResourcesWithCacheHeaders = 0;

  Object.keys(resources)
    .forEach(resourceKey => {

      const resource = resources[resourceKey];

      if (!isResourceCompleted(resource)) return;
      if (!isAsset(resource.res.contentType)) return;

      debug(() => `The resource "${resource.res.url}" is considered static.`);

      totalStaticResources++;
      if (!hasValidCacheHeaders(resource.res)) return;

      debug(() => `The resource "${resource.res.url}" has a cache header.`);
      totalStaticResourcesWithCacheHeaders++;

    })
  ;

  const ratio = totalStaticResourcesWithCacheHeaders/totalStaticResources*100;

  debug(() => `Static resources with cache headers ratio ${ratio.toFixed(1)}%`);

  const status = totalStaticResources === 0 ? NA : ( ratio >= 95 ? OK : NOK );

  return {
    slug: 'cache-headers',
    id: 78,
    status: status,
    data: { ratio, totalStaticResourcesWithCacheHeaders, totalStaticResources },
  };

}

function hasValidCacheHeaders(response) {

  const headers = response.headers;
  let cache = {};
  let isValid = false;

  for (let header, i = 0; (header = headers[i]); ++i) {
    if (header.name === 'Cache-Control') cache.CacheControl = header.value;
    if (header.name === 'Expires') cache.Expires = header.value;
    if (header.name === 'Date') cache.Date = header.value;
  }

  debug(() => `Cache headers gathered: ${JSON.stringify(cache)}`);

  if (cache.CacheControl) {
    if ((/(no-cache)|(no-store)|(max-age\s*=\s*0)/i).test(cache.CacheControl)) {
      debug(() => `Cache-Control header indicate a non cacheable resource: ${cache.CacheControl}`);
      isValid = false;
    } else {
      isValid = true;
    }
  }

  if (cache.Expires) {
    let now = cache.Date ? new Date(cache.Date) : new Date();
    let expires = new Date(cache.Expires);
    // Expires is in the past
    if (expires < now) {
      debug(() => `Expires header is in the past ! ${now.toString()} < ${expires.toString()}`);
      isValid = false;
    }
  }

  return isValid;

}

export default { name, check };
