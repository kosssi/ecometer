import { forEachFrame } from '../helpers.js';
import { OK, NOK } from '../status.js';
// See https://gitlab.com/ecoconceptionweb/ecometer/issues/33

export const name = '19-use-standard-typefaces';
export function check(page) {

  const frames = [];

  forEachFrame(page, () => {
    frames.push({
      url: page.frameUrl,
      customFonts: Array.from(page.evaluate(function() {
        return Array.from(document.styleSheets).reduce((fonts, sheet) => {
          try {
            Array.from(sheet.cssRules).reduce((fonts, cssRule) => {

              // If the rule is not a CSSFont one, skip it
              if (!(cssRule instanceof CSSFontFaceRule)) return fonts;

              // Get the custom font family
              const fontFamily = cssRule.style.getPropertyValue('font-family').replace(/^"|"$/g, '');
              if (!fonts.has(fontFamily)) fonts.add(fontFamily);

              return fonts;

            }, fonts);
          } catch  (err) {
            // Accessing sheet.cssRules will throw a security error if the CSS is loaded from another domain
            if (err.name !== 'SecurityError') throw err;
          }
          return fonts;
        }, new Set());
      })),
    });
  });

  const goal = 0;

  return {
    slug: 'use-standard-typefaces',
    id: 19,
    status: frames.every(f => f.customFonts.length <= goal) ? OK : NOK,
    data: { frames, goal },
  };

}

export default { name, check };
