import { debug, forEachFrame, isMinified, findResourceByUrl } from '../helpers.js';
import { OK, NOK, NA } from '../status.js';
// Check "CSS minification" rule
// See https://gitlab.com/ecoconceptionweb/ecometer/issues/16
export const name = '12-css-minification';
export function check(page, resources) {

  // Search 'sneaky' files (without proper content type)
  const stylesheets = [];
  forEachFrame(page, () => {
    stylesheets.push(...page.evaluate(function() {
      return Array.from(document.styleSheets).reduce((memo, sheet) => {
        // If the referenced stylesheet is inlined but has no associated node, skip it
        if (!sheet.href && !sheet.ownerNode) return memo;
        memo.push(sheet.href
          ? { type: 'external', url: sheet.href }
          : { type: 'inline', content: sheet.ownerNode.textContent }
        );
        return memo;
      }, []);
    }));
  });

  // Search for resource content associated with the stylesheets
  stylesheets.forEach(sheet => {
    if (sheet.type == 'inline') return;
    const resource = findResourceByUrl(resources, sheet.url);
    debug(() => `Searching resource for url "${sheet.url}"`);
    if (resource) sheet.content = resource.res.body;
  });

  let totalCss = stylesheets.length;
  let minifiedCss = 0;

  // Check minification of each stylesheet
  stylesheets.forEach(stylesheet => {
    debug(() => `Detecting CSS minification for "${stylesheet.url ? stylesheet.url : '[INLINED]'}"`);
    if (stylesheet.content && isMinified(stylesheet.content)) minifiedCss++;
  });

  const ratio = totalCss === 0 ? 100 : minifiedCss/totalCss*100;
  const goal = 95; // TODO: refine the goal

  return {
    id: 12,
    slug: 'css-minification',
    status: totalCss === 0 ? NA : ( ratio > goal ? OK : NOK ),
    data: { ratio, minifiedCss, totalCss, goal },
  };

}

export default { name, check };
