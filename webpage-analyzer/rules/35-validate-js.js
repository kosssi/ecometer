import esprima from 'esprima';
import { OK, NOK, NA, reduceStatus } from '../status.js';
import { forEachFrame, findResourceByUrl, isResourceCompleted } from '../helpers.js';
// See https://gitlab.com/ecoconceptionweb/ecometer/issues/34
export const name = '35-validate-js';
export function check(page, resources) {

  const frames = [];

  forEachFrame(page, () => {
    frames.push({
      url: page.frameUrl,
      status: OK,
      scripts: Array.from(page.evaluate(function() {
        return Array.from(document.scripts)
          .map(s => ({ src: s.src, inline: s.textContent }));
      })),
    });
  });

  const goal = 0;

  frames.forEach(f => {
    f.scripts.forEach(script => {

      script.errors = [];

      let code;

      if (!script.inline && script.src) {
        const resource = findResourceByUrl(resources, script.src);
        if (!resource || !isResourceCompleted(resource)) return;
        code = resource.res.body;
      } else if (script.inline) {
        code = script.inline;
        // Truncate inlined content to avoid storing it
        if (script.inline.length > 50) script.inline = script.inline.slice(0, 50)+'...';
      }

      if (!code) return;

      try {
        const syntax = esprima.parse(code, { tolerant: true, sourceType: 'script', loc: true });
        if (syntax.errors) script.errors.push(...syntax.errors);
      } catch (err) {
        script.errors.push(err);
      }

    });

    f.status = f.scripts
        .map(s => s.errors.length <= goal ? OK : NOK)
        .reduce(reduceStatus, NA);
  });



  return {
    slug: 'validate-js',
    id: 35,
    status: frames.map(f => f.status).reduce(reduceStatus, NA),
    data: { frames, goal },
  };

}

export default { name, check };
