import { OK, NOK, NA } from '../status.js';
// See https://gitlab.com/ecoconceptionweb/ecometer/issues/5
export const name = '16-print-css';
export function check(page) {
  const totalPrintStylesheets = page.evaluate(function() {
    // TODO: handle complex media queries with "not" operators
    // See https://developer.mozilla.org/en-US/docs/Web/CSS/Media_Queries/Using_media_queries
    return document.querySelectorAll('link[rel=stylesheet][media~=print]').length +
      document.querySelectorAll('style[media~=print]').length;
  });
  return {
    slug: 'print-css',
    id: 16,
    status: totalPrintStylesheets === 0 ? NA : (totalPrintStylesheets > 0 ? OK : NOK),
  };
}

export default { name, check };
