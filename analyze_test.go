package main

import (
	"bytes"
	"encoding/base64"
	"flag"
	"image/png"
	"net/http"
	"net/http/httptest"
	"testing"

	"go.ltgt.net/net/http/testhandlers"
)

var analyzr analyzer

func init() {
	flag.StringVar(&(analyzr.slimerjs), "slimerjs", "slimerjs", "Name of, or path to the slimerjs binary")
	flag.StringVar(&(analyzr.script), "script", "./webpage-analyzer.js", "Path to the analysis script to run in slimerjs")
}

func TestAnalyzeCommandStartError(t *testing.T) {
	sut := analyzer{slimerjs: "/no-exist-binary"}
	r, _, err := sut.Analyze("http://example.com/")
	if err != nil {
		t.Fatal(err)
	}
	if r.Error == nil {
		t.Fatal("report.Error: expected an error")
	}

	if g, e := r.Error.Name, "COMMAND_START_ERROR"; g != e {
		t.Errorf("report.Error: name = %q, want = %q", g, e)
	}
}

func TestAnalyzeInvalidJSON(t *testing.T) {
	sut := analyzer{slimerjs: "testdata/helper-process.sh"}
	r, _, err := sut.Analyze("http://example.com/")
	if err != nil {
		t.Fatal(err)
	}
	if r.Error == nil {
		t.Fatal("report.Error: expected an error")
	}

	if g, e := r.Error.Name, "INVALID_JSON"; g != e {
		t.Errorf("report.Error: name = %q, want = %q", g, e)
	}
}

func TestAnalyze(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	if !flag.Parsed() {
		flag.Parse()
	}

	server := httptest.NewServer(testhandlers.AddHeaders(testhandlers.Delay(http.FileServer(http.Dir("testdata")))))
	defer server.Close()

	// That additional Run is needed to block until all parallel subtests are executed
	// Otherwise, the server would be Closed before the subtests actually run.
	t.Run("parallel", func(t *testing.T) {
		testdata := []struct {
			path             string
			additionalChecks func(*testing.T, *report)
		}{
			{"standalone.html", nil}, // TODO: more tests
			{"basic-page/", nil},     // TODO: more tests (e.g. external stylesheet)
			{"recurring-ajax/", nil}, // TODO: more tests (e.g. triggered global timeout)
			{"externalize-css-and-js/", test7ExternalizeCSSAndJS},
			{"basic-page/", test10LimitCSS},
			{"css-minification/", test12CssMinification},
			{"print-css/link.html", test16PrintCSS},
			{"print-css/style.html", test16PrintCSS},
			{"use-standard-typefaces/", test19UseStandardTypefaces},
			{"plugins/", test24LimitPlugins},
			{"validate-js/", test35ValidateJS},
			{"minimize-number-of-domains/", test170MinimizeNumberOfDomains},
			{"basic-page/", test180LimitHTTPRequests},
		}

		for _, d := range testdata {
			d := d // capture range variable
			t.Run(d.path, func(t *testing.T) {
				t.Parallel()
				r, _, err := analyzr.Analyze(server.URL + "/" + d.path)
				if err != nil {
					t.Fatal(err)
				}
				if r.Error != nil {
					t.Fatalf("report.Error: unexpected error %#v", r.Error)
				}

				checkScreenshot(t, r)
				if len(r.Rules) == 0 {
					t.Fatal("report.Rules: got empty slice")
				}
				for _, rr := range r.Rules {
					if rd, ok := rulesBySlug[rr.Slug]; !ok {
						t.Errorf("report.Rules: rule description not found for slug %q", rr.Slug)
					} else {
						if g, e := rr.ID, rd.ID; g != e {
							t.Errorf("report.Rules: rule ID doesn't match between report (%d) and description (%d)", g, e)
						}
					}
				}
				if d.additionalChecks != nil {
					d.additionalChecks(t, r)
				}
			})
		}

		testerrordata := []string{
			"standalone.html?delay=12s",
		}
		for _, p := range testerrordata {
			p := p // capture range variable
			t.Run(p, func(t *testing.T) {
				t.Parallel()
				r, _, err := analyzr.Analyze(server.URL + "/" + p)
				if err != nil {
					t.Fatal(err)
				}
				if r.Error == nil {
					t.Fatal("report.Error: expected an error")
				}

				if g, e := r.Error.Name, "PAGE_LOAD_FAILED"; g != e {
					t.Errorf("report.Error: name = %q, want = %q", g, e)
				}
				if r.Error.Message == "" {
					t.Error("report.Error: got empty message")
				}
				if r.Screenshot != "" {
					t.Error("report.Screenshot: got non-empty string on error")
				}
				if len(r.Rules) != 0 {
					t.Error("report.Rules: got non-empty slice on error")
				}
			})
		}
	})
}

func checkScreenshot(t *testing.T, r *report) {
	if r.Screenshot == "" {
		t.Error("report.Screenshot: got empty string")
	} else if s, err := base64.StdEncoding.DecodeString(r.Screenshot); err != nil {
		t.Error("report.Screenshot: malformed base64", err)
	} else if _, err := png.Decode(bytes.NewReader(s)); err != nil {
		t.Error("report.Screenshot: malformed PNG", err)
	}
}

func test7ExternalizeCSSAndJS(t *testing.T, r *report) {
	rr, err := r.findRuleByID(7)
	if err != nil {
		t.Error(err)
	}
	if g, e := len(rr.Data["frames"].([]interface{})), 1; g != e {
		t.Errorf("externalizeCSSAndJS.Data['frames']: expected: %v, got %v", e, g)
	}
	if g, e := rr.Status, statusNOK; g != e {
		t.Errorf("externalizeCSSAndJS.Status: expected: %v, got: %v", e, g)
	}
}

func test10LimitCSS(t *testing.T, r *report) {
	rr, err := r.findRuleByID(10)
	if err != nil {
		t.Error(err)
	}
	if g, e := len(rr.Data["frames"].([]interface{})), 1; g != e {
		t.Errorf("len(limitCSS.Data.frames): expected: %v, got: %v", e, g)
	}
	if g, e := rr.Status, statusOK; g != e {
		t.Errorf("limitCSS.Status: expected: %v, got: %v", e, g)
	}
}

func test12CssMinification(t *testing.T, r *report) {
	rr, err := r.findRuleByID(12)
	if err != nil {
		t.Error(err)
	}
	if totalCSS, ok := rr.Data["totalCss"].(float64); !ok || totalCSS != 4 {
		t.Errorf("minifiedCSSRuleResult.Data.totalCss: expected: 4, got: %v", rr.Data["totalCss"])
	}
	if minifiedCSS, ok := rr.Data["minifiedCss"].(float64); !ok || minifiedCSS != 2 {
		t.Errorf("minifiedCSSRuleResult.Data.minifiedCss: expected: 2, got: %v", rr.Data["minifiedCss"])
	}
	if ratio, ok := rr.Data["ratio"].(float64); !ok || ratio != 50 {
		t.Errorf("minifiedCSSRuleResult.Data.ratio: expected: 50, got: %v", rr.Data["ratio"])
	}
	if g, e := rr.Status, statusNOK; g != e {
		t.Errorf("minifiedCSSRuleResult.Status: expected: %v, got: %v", e, g)
	}
}

func test16PrintCSS(t *testing.T, r *report) {
	rr, err := r.findRuleByID(16)
	if err != nil {
		t.Error(err)
	}
	if g, e := rr.Status, statusOK; g != e {
		t.Errorf("printCss.Status: expected %v, got %v", e, g)
	}
}

func test19UseStandardTypefaces(t *testing.T, r *report) {
	rr, err := r.findRuleByID(19)
	if err != nil {
		t.Error(err)
	}
	if g, e := len(rr.Data["frames"].([]interface{})), 1; g != e {
		t.Errorf("len(useStandardTypefaces.Data['frames']): expected %v, got %v", e, g)
	}
	if g, e := rr.Status, statusNOK; g != e {
		t.Errorf("useStandardTypefaces.Status: expected %v, got %v", e, g)
	}
}

func test24LimitPlugins(t *testing.T, r *report) {
	limitPluginsRule, err := r.findRuleByID(24)
	if err != nil {
		t.Error(err)
	}
	if limitPluginsRule.Status == statusOK {
		t.Errorf("limitPluginRule.Status: should be '%s'", statusNOK)
	}
	if len(limitPluginsRule.Data["plugins"].([]interface{})) != 2 {
		t.Error("limitPluginRule.Data['plugins']: length should be 2")
	}
}

func test35ValidateJS(t *testing.T, r *report) {
	rr, err := r.findRuleByID(35)
	if err != nil {
		t.Error(err)
	}
	if g, e := rr.Status, statusNOK; g != e {
		t.Errorf("validateJS.Status: expected %v, got %v", e, g)
	}
	if g, e := len(rr.Data["frames"].([]interface{})), 1; g != e {
		t.Errorf("len(validateJS.Data['frames']): expected %v, got %v", e, g)
	}
}

func test170MinimizeNumberOfDomains(t *testing.T, r *report) {
	rr, err := r.findRuleByID(170)
	if err != nil {
		t.Error(err)
	}
	if g, e := rr.Status, statusOK; g != e {
		t.Errorf("minimizeNumberOfDomains.Status: expected %v, got %v", e, g)
	}
	if g, e := len(rr.Data["domains"].([]interface{})), 2; g != e {
		t.Errorf("len(minimizeNumberOfDomains.Data['domains']): expected %v, got %v", e, g)
	}
}

func test180LimitHTTPRequests(t *testing.T, r *report) {
	rr, err := r.findRuleByID(180)
	if err != nil {
		t.Error(err)
	}
	if g, e := rr.Status, statusOK; g != e {
		t.Errorf("limitHttpRequests.Status: expected %v, got %v", e, g)
	}
	if g, e := rr.Data["totalRequests"].(float64), 3.0; g != e {
		t.Errorf("limitHttpRequests.Data['totalRequests']: expected %v, got %v", e, g)
	}
}
