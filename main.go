package main

import (
	"bytes"
	"compress/gzip"
	"flag"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"net/url"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"github.com/jackc/pgx"
)

var (
	views = template.New("").
		Funcs(template.FuncMap{
			"rule":           func(name string) *rule { return rulesBySlug[name] },
			"rulePassed":     func(rule *ruleReport) bool { return rule.Status == statusOK },
			"ruleApplicable": func(rule *ruleReport) bool { return rule.Status != statusNA },
			"ellipsis":       ellipsis,
		})

	jobs  *jobQueue
	store *reportStore
)

func jobHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		u := strings.TrimSpace(r.PostFormValue("url"))
		if u == "" {
			http.Redirect(w, r, "/", http.StatusSeeOther)
			return
		}
		u, err := normalizeURL(u)
		if err != nil {
			// TODO: HTML response
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		j, err := jobs.Add(u)
		if err != nil {
			// TODO: HTML response
			http.Error(w, fmt.Sprintf("Error enqueuing job for URL %v: %v", u, err), http.StatusInternalServerError)
			return
		}
		http.Redirect(w, r, "/job?url="+url.QueryEscape(j.URL), http.StatusSeeOther)
		return
	}

	u := r.FormValue("url")
	if u == "" {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}
	u, err := normalizeURL(u)
	if err != nil {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}
	j, ok := jobs.Get(u)
	if !ok {
		if h, err := store.HasReport(u); err != nil {
			// TODO: HTML response
			http.Error(w, fmt.Sprintf("Error checking for report for URL %v: %v", u, err), http.StatusInternalServerError)
			return
		} else if h {
			http.Redirect(w, r, "/report?url="+url.QueryEscape(u), http.StatusSeeOther)
			return
		}
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	execTemplate(w, r, http.StatusOK, "job.html", struct {
		Job         job
		SampleRules []*rule
	}{
		Job:         j,
		SampleRules: randomRules(),
	})
}

func reportHandler(w http.ResponseWriter, r *http.Request) {
	u := r.FormValue("url")
	if u == "" {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}
	u, err := normalizeURL(u)
	if err != nil {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}
	rep, err := store.FindLatestForURL(u)
	if err != nil {
		// TODO: HTML response
		http.Error(w, fmt.Sprintf("Error getting report for URL %v: %v", u, err), http.StatusInternalServerError)
		return
	}
	if rep == nil {
		// TODO: 404 page allowing to run analysis of the given URL
		// For now, redirect to job handler, which will either wait for an ongoing job or redirect to the homepage
		http.Redirect(w, r, "/job?url="+url.QueryEscape(u), http.StatusSeeOther)
		return
	}

	type contextSummary struct {
		Passed   int
		Rules    []*ruleReport
		Progress float32
	}
	var passedRules int
	rulesByContext := make(map[string]contextSummary)
	for i := range rep.Report.Rules {
		r := &rep.Report.Rules[i]
		c := string(rulesBySlug[r.Slug].Context)
		cs := rulesByContext[c]
		if r.Status == statusOK {
			cs.Passed++
			passedRules++
		}
		cs.Rules = append(cs.Rules, r)
		cs.Progress = float32(cs.Passed) / float32(len(cs.Rules)) * 100
		rulesByContext[c] = cs
	}
	execTemplate(w, r, http.StatusOK, "report.html", struct {
		Report         *dbReport
		PassedRules    int
		RulesByContext map[string]contextSummary
	}{
		Report:         rep,
		PassedRules:    passedRules,
		RulesByContext: rulesByContext,
	})
}

func jobStatusAjaxHandler(w http.ResponseWriter, r *http.Request) {
	// Note: the response is so small it's not worth gzipping it.
	u := r.FormValue("url")
	if u == "" {
		http.Error(w, "Missing URL parameter", http.StatusBadRequest)
		return
	}
	u, err := normalizeURL(u)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	if _, ok := jobs.Get(u); ok {
		_, err = io.WriteString(w, "{\"status\":false}")
	} else {
		// TODO: also find report by URL; for now assume no-job==job-completed
		_, err = io.WriteString(w, "{\"status\":true}")
	}
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func main() {
	var addr, staticPath, viewsPath string
	flag.StringVar(&addr, "addr", ":8080", "Address (interface and port) to listen on")
	flag.StringVar(&staticPath, "static", "./static/", "Directory containing the static assets")
	flag.StringVar(&viewsPath, "views", "./views/", "Directory containing the view templates")

	var numWorkers int
	flag.IntVar(&numWorkers, "workers", runtime.GOMAXPROCS(0), "Number of analysis workers")

	var analyzr analyzer
	flag.StringVar(&(analyzr.slimerjs), "slimerjs", "slimerjs", "Name of, or path to the slimerjs binary")
	flag.StringVar(&(analyzr.script), "script", "./webpage-analyzer.js", "Path to the analysis script to run in slimerjs")

	var dsn string
	flag.StringVar(&dsn, "dsn", "", "Data source name of the PostgreSQL database")

	flag.Parse()

	template.Must(views.ParseFiles(
		filepath.Join(viewsPath, "index.html"),
		filepath.Join(viewsPath, "job.html"),
		filepath.Join(viewsPath, "report.html"),
	))

	pgpool, err := createPGConnPool(dsn)
	if err != nil {
		panic(err)
	}

	jobs, err = createJobQueue(pgpool)
	if err != nil {
		panic(err)
	}

	store, err = createReportStore(pgpool)
	if err != nil {
		panic(err)
	}

	http.HandleFunc("/job", jobHandler)
	http.HandleFunc("/report", reportHandler)
	http.HandleFunc("/ajax/job", jobStatusAjaxHandler)
	// TODO: content-negotiation between non-compressed vs. gzipped assets
	fs := http.FileServer(http.Dir(staticPath))
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/" {
			execTemplate(w, r, http.StatusOK, "index.html", randomRules())
			return
		}
		fs.ServeHTTP(w, r)
	})

	go dispatcher(nil, numWorkers, &analyzr)

	// See https://blog.gopheracademy.com/advent-2016/exposing-go-on-the-internet/
	srv := &http.Server{
		Addr:         addr,
		Handler:      recordStats("http", http.DefaultServeMux),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  2 * time.Minute,
	}
	log.Fatal(srv.ListenAndServe())
}

func createPGConnPool(dsn string) (*pgx.ConnPool, error) {
	// Logic borrowed from github.com/lib/pq
	var c pgx.ConnConfig
	var err error
	if dsn == "" {
		c, err = pgx.ParseEnvLibpq()
	} else if strings.HasPrefix(dsn, "postgres://") || strings.HasPrefix(dsn, "postgresql://") {
		c, err = pgx.ParseURI(dsn)
	} else {
		c, err = pgx.ParseDSN(dsn)
	}
	if err != nil {
		return nil, err
	}

	return pgx.NewConnPool(pgx.ConnPoolConfig{
		ConnConfig: c,
		// TODO: connection pool configuration
	})
}

func execTemplate(w http.ResponseWriter, r *http.Request, status int, name string, data interface{}) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	cw := compress(w, r)
	w.WriteHeader(status)
	if err := views.ExecuteTemplate(cw, name, data); err != nil {
		log.Println(err)
	}
	if cwc, ok := cw.(io.Closer); ok {
		_ = cwc.Close()
	}
}

func compress(w http.ResponseWriter, r *http.Request) io.Writer {
	// Note: this is an approximation; it notably does not respect qvalues.
	// In practice, major browsers do not send qvalues.
	ae := r.Header.Get("Accept-Encoding")
	isSeparator := func(b byte) bool {
		return strings.IndexByte(" \t;,", b) >= 0
	}
	// TODO: "br" when there's a pure-Go Brotli implementation
	if i := strings.Index(ae, "gzip"); i >= 0 &&
		(i == 0 || isSeparator(ae[i-1])) &&
		(i+4 == len(ae) || isSeparator(ae[i+4])) {
		w.Header().Set("Content-Encoding", "gzip")
		return gzip.NewWriter(w)
	}
	return w
}

func ellipsis(text string, maxLength int) string {
	// XXX: this assumes the input only contains US-ASCII
	textLength := len(text)
	overflow := textLength - maxLength

	if overflow <= 0 {
		return text
	}

	middle := int(textLength / 2)
	overflow = overflow/2 + 2

	var buf bytes.Buffer
	buf.WriteString(text[:middle-overflow])
	buf.WriteRune('…')
	buf.WriteString(text[middle+overflow:])
	return buf.String()
}
