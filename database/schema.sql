-- Schema creation

DROP SCHEMA IF EXISTS ecometer CASCADE;

CREATE SCHEMA ecometer;

SET SCHEMA 'ecometer';

-- Reports table definition

CREATE TABLE reports (
  id SERIAL PRIMARY KEY,
  report JSON NOT NULL,
  url TEXT NOT NULL,
  sys_usage JSON,
  job_creation_timestamp TIMESTAMPTZ NOT NULL,
  job_start_timestamp TIMESTAMPTZ NOT NULL,
  creation_timestamp TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- Jobs table definition

CREATE TABLE jobs (
  url TEXT PRIMARY KEY,
  creation_timestamp TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  job_start_timestamp TIMESTAMPTZ DEFAULT NULL
);
