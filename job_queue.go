// Copyright (C) 2016  The EcoMeter authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"fmt"
	"time"

	"github.com/jackc/pgx"
)

const (
	stmtFindJob   = "find_job"
	stmtInsertJob = "insert_job"
	stmtTakeJob   = "take_job"
	stmtDeleteJob = "delete_job"
	stmtReturnJob = "return_job"
	stmtQueueSize = "queue_size"
)

type job struct {
	URL     string
	Created time.Time
	Started time.Time
}

type jobQueue struct {
	pool *pgx.ConnPool
}

func createJobQueue(pgpool *pgx.ConnPool) (*jobQueue, error) {
	q := &jobQueue{
		pool: pgpool,
	}

	if _, err := q.pool.Prepare(stmtFindJob, `
SELECT url, creation_timestamp, job_start_timestamp FROM ecometer.jobs WHERE url = $1
`); err != nil {
		return nil, err
	}
	if _, err := q.pool.Prepare(stmtInsertJob, `
INSERT INTO ecometer.jobs (url) VALUES ($1)
RETURNING url, creation_timestamp, job_start_timestamp
`); err != nil {
		return nil, err
	}
	if _, err := q.pool.Prepare(stmtTakeJob, `
UPDATE ecometer.jobs
SET job_start_timestamp = CURRENT_TIMESTAMP
WHERE url = (
	SELECT url
	FROM ecometer.jobs
	WHERE job_start_timestamp IS NULL
	ORDER BY creation_timestamp
	LIMIT 1
)
RETURNING url, creation_timestamp, job_start_timestamp
`); err != nil {
		return nil, err
	}
	if _, err := q.pool.Prepare(stmtDeleteJob, `
DELETE FROM ecometer.jobs
WHERE url = $1
  AND job_start_timestamp IS NOT NULL
`); err != nil {
		return nil, err
	}
	if _, err := q.pool.Prepare(stmtReturnJob, `
UPDATE ecometer.jobs
SET job_start_timestamp = NULL
WHERE url = $1
  AND job_start_timestamp IS NOT NULL
`); err != nil {
		return nil, err
	}
	if _, err := q.pool.Prepare(stmtQueueSize, `
SELECT COUNT(*) AS all, COUNT(job_start_timestamp) AS running
FROM ecometer.jobs
`); err != nil {
		return nil, err
	}

	return q, nil
}

// clear empties the job queue
func (q *jobQueue) clear() {
	q.pool.Exec("DELETE FROM ecometer.jobs")
}

// Add enqueues a job for the given URL, unless one already exists for the same URL.
// It returns a non-nil error if the job is rejected (e.g. the queue is too big),
// or returns the enqueued job (either a new or existing one) otherwise.
func (q *jobQueue) Add(url string) (j job, err error) {
	var started *time.Time
	// TODO: max out number of queued jobs (this would require a SELECT befort the INSERT)
	err = q.pool.QueryRow(stmtInsertJob, url).Scan(&j.URL, &j.Created, &started)
	if pe, ok := err.(pgx.PgError); ok && pe.Code == "23505" {
		// Job already queued for the URL, select it
		err = q.pool.QueryRow(stmtFindJob, url).Scan(&j.URL, &j.Created, &started)
	}
	if started != nil {
		j.Started = *started
	}
	return
}

// Get returns a job from the queue for the given URL.
// The returned boolean indicates whether the queue contains such a job or not
// (which can either mean that the job is finished, or that no job has been enqueued!)
func (q *jobQueue) Get(url string) (job, bool) {
	var j job
	var started *time.Time
	err := q.pool.QueryRow(stmtFindJob, url).Scan(&j.URL, &j.Created, &started)
	if started != nil {
		j.Started = *started
	}
	return j, (err == nil)
}

// Take marks a job as being started and returns it.
// The returned error will be non-nil if the job queue is empty.
// The job will have to be marked either completed or failed.
func (q *jobQueue) Take() (j job, err error) {
	err = q.pool.QueryRow(stmtTakeJob).Scan(&j.URL, &j.Created, &j.Started)
	return
}

// MarkCompleted marks the job as completed, effectively removing it from the queue entirely.
func (q *jobQueue) MarkCompleted(url string) error {
	r, err := q.pool.Exec(stmtDeleteJob, url)
	if err != nil {
		return err
	}
	if r.RowsAffected() == 0 {
		return fmt.Errorf("No job in queue for %s, or job was not started", url)
	}
	return nil
}

// MarkFailed puts the job back to the queue, ready to be taken again.
func (q *jobQueue) MarkFailed(url string) error {
	r, err := q.pool.Exec(stmtReturnJob, url)
	if err != nil {
		return err
	}
	if r.RowsAffected() == 0 {
		return fmt.Errorf("No job in queue for %s, or job was not started", url)
	}
	return nil
}

func (q *jobQueue) Size() (queued, running uint, err error) {
	err = q.pool.QueryRow(stmtQueueSize).Scan(&queued, &running)
	queued -= running // the query actually returns the count of all jobs, not queued ones
	return
}
