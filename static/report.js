function toggleClass(theElement, theClass) {
    if (theElement.classList) {
        theElement.classList.toggle(theClass);
    } else {
        var classes = theElement.className.split(' ');
        var existingIndex = classes.indexOf(theClass);
        if (existingIndex >= 0) {
            classes.splice(existingIndex, 1);
        } else {
            classes.push(theClass);
        }
        theElement.className = classes.join(' ');
    }
}

function hasClass(theElement, theClass) {
    if (theElement.classList) {
        theElement.classList.contains(theClass);
    } else {
        new RegExp('(^| )' + theClass + '( |$)', 'gi').test(theElement.className);
    }
}

function ready(fn) {
    if (document.readyState != 'loading') {
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}


var toggleRulesList = document.querySelectorAll('.rules-list .card .toggle');
Array.prototype.forEach.call(toggleRulesList, function(el, i) {
    el.onclick = function() {
        toggleClass(el, 'active');
    };
});

var toggleMenu = document.querySelectorAll('header .toggle');
var navMenu = document.querySelectorAll('nav.menu');
toggleMenu[0].onclick = function() {
    toggleClass(navMenu[0], 'open');
};
