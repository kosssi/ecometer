var $toggle = document.querySelector('.toggle');
var $menu = document.querySelector('.menu');
$toggle.onclick = function(){
  if ($menu.classList) {
    $menu.classList.toggle('open');
  } else {
    var classes = $menu.className.split(' ');
    var existingIndex = classes.indexOf('open');
    if (existingIndex >= 0) {
      classes.splice(existingIndex, 1);
    } else {
      classes.push('open');
    }
    $menu.className = classes.join(' ');
  }
};
