import commonjs from 'rollup-plugin-commonjs';
import nodeResolve from 'rollup-plugin-node-resolve';

export default {
  plugins: [
    nodeResolve({ main: true, jsnext: true }),
    commonjs(),
  ],
  entry: 'webpage-analyzer/index.js',
  format: 'cjs',
  // PhantomJS core modules
  external: [ 'system', 'fs', 'child_process', 'webpage', 'webserver' ],
  dest: 'webpage-analyzer.js',
};
